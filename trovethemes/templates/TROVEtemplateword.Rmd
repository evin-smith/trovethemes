---
title: "Test"
author: "Ev Smith"
date: '`r Sys.Date()`'
toc : true
always_allow_html: yes
output: 
  word_document:
    reference_doc : TROVEtemplateword.docx
knit: (function(inputFile, encoding) {  rmarkdown::render(inputFile,
  encoding = encoding,
  output_dir = "\\Users\\EvinSmith-TROVE\\Desktop\\Rmarkdown Template") }) # specify your own output directory here!
---


# Table of contents heading 1
## Subsection of heading 1 
content for the subsection here
```{r setup, include=FALSE}

knitr::opts_chunk$set(echo = TRUE)
knitr::opts_chunk$set(warning = FALSE)
knitr::opts_chunk$set(message = FALSE)
library(tinytex)
```

# plots
```{r}
rm(list = ls())
library(trove)
ggplot(iris) + 
  geom_point(aes(x = Sepal.Length, y = Sepal.Width, 
                              color = Sepal.Width)) +
  trove_black() +
  scale_color_continuous_trove()

ggplot(iris) +
  geom_point(aes(x = Sepal.Length, y = Sepal.Width, color = Species)) +
  trove_black() + 
  scale_color_discrete_trove()

ggplot(iris) + 
  geom_bar(aes(x = Sepal.Length), fill= trovegreen) +
  trove_black()

ggplot(iris) +
  geom_boxplot(aes(x = Sepal.Length,y= Sepal.Width), fill= trovegreen, color = "#fafafa") +
  trove_black()

ggplot(iris) +
  geom_point(aes(x = Sepal.Length,y= Sepal.Width), color= trovegreen) +
  trove_black() + 
  scale_color_continuous_trove()



```


