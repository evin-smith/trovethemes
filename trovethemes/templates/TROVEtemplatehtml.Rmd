---
title: "Untitled"
author: "Evin Smith"
date: "1/21/2020"
output:
  html_document:
    css: trovelight.css
    highlight: null
    theme: null
  word_document: default
---

```{r setup, fig.align='center', echo=FALSE}
library(knitr)
include_graphics("trovelogo.png")
```

```{r}
rm(list = ls())
library(trove)
ggplot(iris) + 
  geom_point(aes(x = Sepal.Length, y = Sepal.Width, 
                              color = Sepal.Width)) +
  trove_black() +
  scale_color_continuous_trove()

ggplot(iris) +
  geom_point(aes(x = Sepal.Length, y = Sepal.Width, color = Species)) +
  trove_black() + 
  scale_color_discrete_trove()

ggplot(iris) + 
  geom_bar(aes(x = Sepal.Length), fill= trovegreen) +
  trove_black()

ggplot(iris) +
  geom_boxplot(aes(x = Sepal.Length,y= Sepal.Width), fill= trovegreen, color = "#fafafa") +
  trove_black()

ggplot(iris) +
  geom_point(aes(x = Sepal.Length,y= Sepal.Width), color= trovegreen) +
  trove_black() + 
  scale_color_continuous_trove()



```


```{r}
table_dark(mtcars)
table_light(mtcars)
```


