# troveThemes
## To use package:


# Step 1:
Download troveThemes.tar.gz file



# Step 2: 
In RStudio go to tools>install package, in install from: select package archive from drop down, 
then browse to where troveThemes.tar.gz is saved and select, hit install, like so:

`install.packages("troveThemes.tar.gz", repos = NULL, type = "source")`


# Step 3:
Call `library(troveThemes)` to use package


# Step 4:
Vingette is found by typing `??troveThemes` and clicking the link.
